$(document).ready(function() {
    $(document).on('submit', "#parsing_form, #parsing_form_min_data", function () {
        var data = {};
        let type = {};
        var form_data = $(this).serializeArray();
        var form = $(this);
        var error = [];

        $.each(form_data, function (key, input) {
            if(!input.value){
                error.push(input.name);
            }else if(input.name=="page" && Number.isInteger(Number(input.value)) == false){
                error.push(input.name);
            }else{
                if(input.name=="type_allocation"){
                    if(data[input.name]){
                        data[input.name] += input.value+";";
                    }else{
                        data[input.name] = input.value+";";
                    }
                }else{
                    data[input.name] = input.value;
                }
            }
        });

        form.find("input").removeClass("error");
        if(Array.isArray(error) && error.length){
            $.each(error, function (key, name) {
                form.find("input[name='"+name+"']").addClass("error");
            });
        }else{
            $("#parsing_form, #parsing_form_min_data").hide();
            $(".parsing_process").show();
            console.log(data);
            stepParsing(false, false, data);
        }


        return false;

    });
    $(document).on('click', "#refresh", function () {
        location.reload();
    });
});
function stepParsing(start_page, end_page, data){

    let newDate = {};
    var percent = 0;

    if(start_page && end_page){
        percent = Math.round(start_page*100/end_page);
        newDate["page"] = start_page;
    }else{
        newDate["city_region"] = data["city_region"];
        newDate["dest_type"] = data["dest_type"];
        newDate["type_allocation"] = data["type_allocation"];
    }

    newDate["url"] = data["url"];
    newDate["item"] = data["item"];
    newDate["upload_path"] = data["upload_path"];
    newDate["type_file"] = data["type_file"];
    newDate["upload_img"] = data["upload_img"];
    newDate["mindata"] = data["mindata"];

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "ajax.php",
        data: newDate,
        success: function (datajson) {

            if(datajson.ERROR){

                $(".error_block").html(datajson.ERROR).show();

            }else{

                $(".parsing_process .percent").html(percent+"%");
                $(".parsing_process progress").attr("value", percent);

                if(datajson.COUNT_PAGE && datajson.FIRST_LINK){

                    data["url"] = datajson.FIRST_LINK;
                    $(".log").append("City or Region:"+data["city_region"]+";<br>Count page:"+datajson.COUNT_PAGE+";<br>link first page:"+datajson.FIRST_LINK+"<br>");
                    stepParsing(1, datajson.COUNT_PAGE, data);

                }else{
                    if(start_page == end_page){
                        $(".log").append(datajson);
                        $(".success").show();
                    }else{
                        $(".log").append(datajson);
                        start_page++;
                        stepParsing(start_page, end_page, data);
                    }

                }
            }

        }
    });

}