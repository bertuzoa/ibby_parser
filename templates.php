<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <title>Parsing Booking.com</title>
    <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="script.js"></script>
</head>
<body>
<h1>Parsing Booking.com</h1>
<div class="parsing_block">
    <div id="refresh" class="refresh">&#8635;</div>
    <form id="parsing_form" action="" enctype="multipart/form-data" method="post">
        <h2>Parsing form</h2>
        <div class="calculation__form-wrapper">
            <div class="calculation__field  small_field">
                <label class="calculation__field-desc" for="dest_type">Destination type<span>*</span> </label>
                <select name="dest_type" id="dest_type">
                    <option value="city" selected>City</option>
                    <option value="region">Region</option>
                </select>
            </div>
            <div class="calculation__field  small_field">
                <label class="calculation__field-desc" for="city_region">City or Region<span>*</span> </label>
                <input data-type="text" class="calculation__field-input" name="city_region" type="text" id="city_region">
            </div>
            <div class="calculation__field  big_field">
                <label class="calculation__field-desc" for="type_allocation">Type allocation<span>*</span> </label>
                <select name="type_allocation" id="type_allocation" multiple size="4">
                    <option value="201">Апартаменты</option>
                    <option value="204" selected>Отели</option>
                    <option value="203" selected>Хостелы</option>
                    <option value="216">Гостевые дома</option>
                    <option value="222">Проживание в семье</option>
                    <option value="208" selected>Отели типа «постель и завтрак»</option>
                    <option value="225" selected>Капсульные отели</option>
                    <option value="226">Отели для свиданий</option>
                    <option value="220">Дома для отпуска</option>
                    <option value="223">Загородные дома</option>
                    <option value="212">Комплексы для отдыха с коттеджами/бунгало</option>
                    <option value="214">Кемпинги</option>
                    <option value="231" selected>Отели эконом-класса</option>
                    <option value="205" selected>Мотели</option>
                    <option value="221">Лоджи</option>
                    <option value="224">Люкс-шатры</option>
                    <option value="228">Шале</option>
                    <option value="233">Санатории</option>
                </select>
            </div>
            <div class="calculation__field small_field">
                <label class="calculation__field-desc" for="item">Count items page<span>*</span></label>
                <input data-type="text" class="calculation__field-input" name="item" type="text" id="item" value="25" readonly>
            </div>
            <div class="calculation__field small_field">
                <label class="calculation__field-desc" for="upload_path">Upload path</label>
                <input data-type="text" class="calculation__field-input" name="upload_path" type="text" id="upload_path" value="/ibby_parser/upload/">
            </div>
            <div class="calculation__field small_field">
                <label class="calculation__field-desc" for="type_file">Type file</label>
                <select name="type_file" id="type_file">
                    <option value="json" selected>json</option>
                    <option value="csv">csv</option>
                </select>
            </div>
            <div class="calculation__field small_field">
                <label class="calculation__field-desc" for="upload_img">Upload images</label>
                <select name="upload_img" id="upload_img">
                    <option value="true" selected>yes</option>
                    <option value="false">no</option>
                </select>
            </div>
        </div>
        <div class="calculation__field">
            <button class="calculation__field-btn" type="sumit">Send</button>
        </div>
    </form>
    <div class="parsing_process">
        <h2>Parsing process</h2>
        <div class="percent">0%</div>
        <progress value="0" max="100"></progress>
        <div class="success">Parsing success</div>
        <div class="error_block"></div>
        <div class="log"></div>
    </div>
</div>
</body>
</html>
